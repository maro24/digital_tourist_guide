class Place {
  int id;
  String name;

  double lat;
  double lon;

  String description;
  int areaId;
  int categoryId;

  Place(
      {this.id,
      this.name,
      this.lat,
      this.lon,
      this.description,
      this.areaId,
      this.categoryId});

  factory Place.fromJson(Map<String, dynamic> json) {
    return Place(
        id: int.parse(json['ID']),
        name: json['name'],
        lat: double.parse(json['lat']),
        lon: double.parse(json['lon']),
        description: json['description'],
        areaId: int.parse(json['area_id']),
        categoryId: int.parse(json['category_id']));
  }
}
