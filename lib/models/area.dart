class Area {
  int id;
  String name;
  //List<double> coordinates;

  /*Area({this.name, this.id, this.coordinates});*/

  Area({this.id, this.name});

  /*factory Area.fromJson(Map<String, dynamic> json){
    return Area(name: json['name'], id: json['id'], coordinates: json['coordinates']);
  }*/

  factory Area.fromJson(Map<String, dynamic> json){
    return Area(id: int.parse(json['ID']), name: json['name']);
  }
}