import 'dart:convert';
import 'package:tourguide/models/image_model.dart';
import 'package:http/http.dart' as http;

class Images {
  static String baseUrl =
      'https://spoznavacka.rohrovi.cz/toursman_demo_api/api1';

  static Future<int> insertImageToDb(ImageModel image) async {
    final response =
        await http.post(baseUrl + '/add_img_db.php', body: imageToMap(image));
    return 1;
  }

  static Map<String, String> imageToMap(ImageModel image) {
    Map<String, String> map = {
      'name': image.name,
      'place_id': image.placeId.toString()
    };
    return map;
  }

  static Future<String> getImageNameByPlaceId(int placeId) async {
    final response =
        await http.get(baseUrl + '/getImgNameByPlaceId.php?place_id=$placeId');
    final decoded = json.decode(response.body)[0]['name'];
    return decoded;
  }

  static Future<int> unlinkImageInDb(int placeId) async {
    final response = await http.post(baseUrl + '/unlinkImage.php',
        body: {'place_id': placeId.toString()});
    return 1;
  }
}
