import 'package:http/http.dart' as http;
import 'package:tourguide/models/category.dart';
import 'dart:convert';

class Categories {
  static String baseUrl = 'https://spoznavacka.rohrovi.cz/toursman_demo_api/api1';

  static Future<String> fetchAllCategoriesFromNet() async {
    final response =
    await http.get(baseUrl + '/getAllCategories.php');
    return response.body;
  }

  static Future<List<Category>> getCategoriesList() async {
    var _categoriesList = new List<Category>();
    final _response = json.decode(await fetchAllCategoriesFromNet());
    for (int i = 0; i < _response.length; i++) {
      _categoriesList.add(new Category.fromJson(_response[i]));
    }
    return _categoriesList;
  }
}