import 'package:http/http.dart' as http;
import 'package:tourguide/models/place.dart';
import 'dart:convert';

class Places {
  static String baseUrl =
      'https://spoznavacka.rohrovi.cz/toursman_demo_api/api1';

  static Future<String> fetchAllPlacesFromNet() async {
    final response = await http.get(baseUrl + '/getAllPlaces.php');
    return response.body;
  }

  static Future<List<Place>> getPlacesList() async {
    var _placesList = new List<Place>();
    final _response = json.decode(await fetchAllPlacesFromNet());
    for (int i = 0; i < _response.length; i++) {
      _placesList.add(new Place.fromJson(_response[i]));
    }
    return _placesList;
  }

  static Map<String, String> placeToMap(Place place) {
    Map<String, String> map = {
      'name': place.name,
      'lat': place.lat.toString(),
      'lon': place.lon.toString(),
      'description': place.description,
      'area_id': place.areaId.toString(),
      'category_id': place.categoryId.toString()
    };
    return map;
  }

  static Map<String, String> placeToMapWithId(Place place) {
    Map<String, String> map = {
      'name': place.name,
      'lat': place.lat.toString(),
      'lon': place.lon.toString(),
      'description': place.description,
      'area_id': place.areaId.toString(),
      'category_id': place.categoryId.toString(),
      'id': place.id.toString()
    };
    return map;
  }

  static Future<int> createPlace(Place place) async {
    final response =
        await http.post(baseUrl + '/createPlace.php', body: placeToMap(place));
    var insertedId = await getLastInsertedPlaceId();
    return insertedId;
  }

  static Future<int> getLastInsertedPlaceId() async {
    final response = await http.get(baseUrl + '/getLastPlaceId.php');
    final decoded = json.decode(response.body);
    return int.parse(decoded[0]['ID']);
  }

  static Future<bool> deletePlace(int placeId) async {
    final response = await http
        .post(baseUrl + '/deletePlace.php', body: {'id': placeId.toString()});
    return true;
  }

  static Future<bool> updatePlace(Place place) async {
    final response = await http.post(baseUrl + '/updatePlace.php',
        body: placeToMapWithId(place));
    return true;
  }
}
