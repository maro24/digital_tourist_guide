import 'package:tourguide/models/area.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Areas {
  static String baseUrl = 'https://spoznavacka.rohrovi.cz/toursman_demo_api/api1';

  static Future<String> fetchAllAreasFromNet() async {
    final response =
        await http.get(baseUrl + '/getAllAreas.php');
    return response.body;
  }

  static Future<List<Area>> getAreasList() async {
    var _areasList = new List<Area>();
    final _response = json.decode(await fetchAllAreasFromNet());
    for (int i = 0; i < _response.length; i++) {
      _areasList.add(new Area.fromJson(_response[i]));
    }
    return _areasList;
  }

  static Future<int> getAreaId(String areaName) async {
    final response = await http.get(
        baseUrl + '/getAreaIdByName?area_name=$areaName');
    return int.parse(json.decode(response.body)[0]['ID']);
  }

  static Future<String> getBoundaryPoints(String areaId) async {
    final response = await http.get(
        baseUrl + '/getBoundaryPointsForId.php?area_id=$areaId');
    return response.body;
  }
}
