import 'package:flutter/material.dart';
import 'pages/home.dart';
import 'pages/areas_list_page.dart';
import 'pages/place_editor_page.dart';
import 'pages/places_list_page.dart';
import 'pages/map_place_selector.dart';
import 'pages/place_view.dart';
import 'pages/filters_page.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
          primaryColor: Colors.green,
          accentColor: Colors.greenAccent,
          primarySwatch: Colors.green),
      home: HomePage(),
      routes: <String, WidgetBuilder>{
        '/home': (BuildContext context) => new HomePage(),
        '/areaList': (BuildContext context) => new AreasListPage(),
        '/placesList': (BuildContext context) => new PlacesListPage(),
        '/placeEdit': (BuildContext context) => new PlaceEditorPage(),
        '/mapSelector': (BuildContext context) => new MapPlaceSelectorPage(),
        '/placeView': (BuildContext context) => new PlaceViewPage(),
        '/filters': (BuildContext context) => new FiltersPage()
      },
    );
  }
}
