class FiltersArguments {
  List<String> areasIdToView;
  List<String> categoriesIdToView;

  FiltersArguments(this.areasIdToView, this.categoriesIdToView);
}
