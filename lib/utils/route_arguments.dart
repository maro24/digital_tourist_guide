class RouteArguments {
  final int areaId;
  final String title;

  RouteArguments(this.areaId, this.title);
}
