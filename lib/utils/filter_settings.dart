class FilterSettings {
  bool filtersSet = false;
  Map<int, bool> areaIds = new Map();
  Map<int, bool> categoryIds = new Map();

  static final FilterSettings _filterSettings = FilterSettings._internal();

  factory FilterSettings() {
    return _filterSettings;
  }

  FilterSettings._internal();
}