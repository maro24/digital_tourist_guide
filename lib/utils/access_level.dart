class AccessLevel {
  bool admin = false;
  static final AccessLevel _accessLevel = AccessLevel._internal();

  factory AccessLevel() {
    return _accessLevel;
  }

  AccessLevel._internal();
}