import 'package:latlong/latlong.dart';

class MapPositionResponse {
  LatLng latLng;

  MapPositionResponse(this.latLng);
}