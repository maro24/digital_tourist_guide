import 'package:flutter/material.dart';
import 'package:tourguide/controllers/area_controller.dart';
import 'package:tourguide/models/area.dart';
import 'dart:convert';
import 'package:tourguide/utils/route_arguments.dart';

class AreasListPage extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return _AreasListPageState();
  }
}

class _AreasListPageState extends State<AreasListPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Oblasti'),
        ),
        body: Center(
          child: FutureBuilder<List<Area>>(
            future: Areas.getAreasList(),
            builder:
                (BuildContext context, AsyncSnapshot<List<Area>> snapshot) {
              switch (snapshot.connectionState) {
                case ConnectionState.none:
                  return Text('Press button to start.');
                case ConnectionState.active:
                case ConnectionState.waiting:
                  return CircularProgressIndicator();
                case ConnectionState.done:
                  if (snapshot.hasError)
                    return Text('Error: ${snapshot.error}');
                  return Column(children: <Widget>[
                    Expanded(
                      child: ListView.builder(
                          itemCount: snapshot.data.length,
                          itemBuilder: (BuildContext context, int Index) {
                            return Card(
                                child: ListTile(
                              leading: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(snapshot.data[Index].name)
                                ],
                              ),
                              trailing: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  IconButton(
                                    iconSize: 32.0,
                                    icon: Icon(
                                      Icons.list,
                                      color: Colors.grey,
                                    ),
                                    onPressed: () {
                                      openPlacesList(snapshot.data[Index].id,
                                          snapshot.data[Index].name);
                                    },
                                  ),
                                ],
                              ),
                              onTap: () {
                                handleClick(snapshot.data[Index].name);
                              },
                            ));
                          }),
                    )
                  ]);
              }
              return null; // unreachable
            },
          ),
        ));
  }

  void openPlacesList(int areaId, String title) {
    Navigator.of(context)
        .pushNamed('/placesList', arguments: RouteArguments(areaId, title));
  }

  void handleClick(String areaName) async {
    String id = (await Areas.getAreaId(areaName)).toString();
    var response = jsonDecode(await Areas.getBoundaryPoints(id));
    Navigator.of(context).pop(response);
  }
}
