import 'package:flutter/material.dart';
import 'package:tourguide/models/category.dart';
import 'package:tourguide/controllers/category_controller.dart';
import 'package:tourguide/models/area.dart';
import 'package:tourguide/controllers/area_controller.dart';
import 'package:tourguide/models/filter_model.dart';
import 'package:tourguide/utils/filter_settings.dart';

class FiltersPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return FiltersPageState();
  }
}

class FiltersPageState extends State<FiltersPage> {
  List<Area> areas = new List();
  List<Category> categories = new List();
  final itemTextStyle = new TextStyle(color: Colors.black);
  FilterSettings filterSettings = FilterSettings();

  @override
  void initState() {
    super.initState();

    loadAreas();
    loadCategories();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Filtry'),
        ),
        body: areas.length > 0 || categories.length > 0
            ? buildLayout()
            : Center(
                child: CircularProgressIndicator(),
              ));
  }

  SingleChildScrollView buildLayout() {
    SingleChildScrollView mainContainer = new SingleChildScrollView(
      padding: EdgeInsets.all(12.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          //Areas
          Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: getAreaChildren()),
          //Categories
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: getCategoryChildren(),
          )
        ],
      ),
    );
    return mainContainer;
  }

  List<Widget> getAreaChildren() {
    List<Widget> children = new List();
    children.add(Container(
      child: Text('Oblasti', style: TextStyle(fontSize: 24.0),),
    ));
    for (final item in areas) {
      filterSettings.areaIds == null
          ? children.add(Container(
              child: Row(
                children: <Widget>[
                  Container(
                      child: Checkbox(
                    activeColor: Theme.of(context).primaryColor,
                    value: true,
                    onChanged: (value) {},
                  )),
                  Container(
                    child: Text(item.name, style: itemTextStyle),
                  )
                ],
              ),
            ))
          : children.add(Container(
              child: Row(
                children: <Widget>[
                  Container(
                      child: Checkbox(
                    activeColor: Theme.of(context).primaryColor,
                    value: filterSettings.areaIds[item.id],
                    onChanged: (value) {
                      setState(() {
                        filterSettings.areaIds[item.id] = value;
                      });
                    },
                  )),
                  Container(
                    child: Text(item.name, style: itemTextStyle),
                  )
                ],
              ),
            ));
    }

    return children;
  }

  List<Widget> getCategoryChildren() {
    List<Widget> children = new List();
    children.add(Container(
      child: Text('Kategorie', style: TextStyle(fontSize: 24.0),),
    ));
    for (final item in categories) {
      filterSettings.categoryIds == null
          ? children.add(Container(
              child: Row(
                children: <Widget>[
                  Container(
                      child: Checkbox(
                    activeColor: Theme.of(context).primaryColor,
                    value: true,
                    onChanged: (value) {},
                  )),
                  getCategoryIcon(item.id),
                  Container(
                    child: Text(item.name, style: itemTextStyle),
                  ),
                ],
              ),
            ))
          : children.add(Container(
              child: Row(
                children: <Widget>[
                  Container(
                      child: Checkbox(
                    activeColor: Theme.of(context).primaryColor,
                    value: filterSettings.categoryIds[item.id],
                    onChanged: (value) {
                      setState(() {
                        filterSettings.categoryIds[item.id] = value;
                      });
                    },
                  )),
                  getCategoryIcon(item.id),
                  Container(
                    child: Text(item.name, style: itemTextStyle),
                  )
                ],
              ),
            ));
    }

    return children;
  }

  Container getCategoryIcon(int categoryId) {
    Icon _categoryIcon;
    switch (categoryId) {
      case 1:
        _categoryIcon = new Icon(
          Icons.local_florist,
          color: Colors.green,
        );
        break;
      case 2:
        _categoryIcon = new Icon(Icons.account_balance, color: Colors.orange);
        break;
      case 3:
        _categoryIcon = new Icon(Icons.wc, color: Colors.blueAccent);
        break;
      case 4:
        _categoryIcon = new Icon(
          Icons.restaurant,
          color: Colors.amber,
        );
        break;
      case 5:
        _categoryIcon = new Icon(
          Icons.bookmark,
          color: Colors.brown,
        );
        break;
      default:
        _categoryIcon = new Icon(Icons.place, color: Colors.red);
        break;
    }
    return Container(
      margin: EdgeInsets.only(right: 8.0),
      child: _categoryIcon,
    );
  }

  void loadAreas() async {
    if (filterSettings.areaIds == null) {
      List<Area> tempAreas = await Areas.getAreasList();
      filterSettings.areaIds = new Map<int, bool>();
      tempAreas.forEach((area) {
        filterSettings.areaIds[area.id] = true;
      });
      setState(() {
        areas = tempAreas;
      });
    } else {
      List<Area> tempAreas = await Areas.getAreasList();
      tempAreas.forEach((area) {
        if (filterSettings.areaIds[area.id] == null) {
          filterSettings.areaIds[area.id] = true;
        }
      });
      setState(() {
        areas = tempAreas;
      });
    }
  }

  void loadCategories() async {
    if (filterSettings.categoryIds == null) {
      List<Category> tempCategories = await Categories.getCategoriesList();
      filterSettings.categoryIds = new Map<int, bool>();
      tempCategories.forEach((category) {
        filterSettings.categoryIds[category.id] = true;
      });
      setState(() {
        categories = tempCategories;
      });
    } else {
      List<Category> tempCategories = await Categories.getCategoriesList();
      tempCategories.forEach((category) {
        if (filterSettings.categoryIds[category.id] == null) {
          filterSettings.categoryIds[category.id] = true;
        }
      });
      setState(() {
        categories = tempCategories;
      });
    }
  }
}
