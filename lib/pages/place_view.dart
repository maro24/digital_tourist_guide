import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:tourguide/models/place.dart';
import 'package:tourguide/utils/access_level.dart';
import 'package:http/http.dart' as http;

class PlaceViewPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return PlaceViewPageState();
  }
}

class PlaceViewPageState extends State<PlaceViewPage> {
  Place placeInfo;
  AccessLevel accessLevel = AccessLevel();
  String imageName;
  bool calledForImage = false;

  Future<void> getImage() async {
    final response = await http.get(
        'https://spoznavacka.rohrovi.cz/toursman_demo_api/api1/getImgNameByPlaceId.php?place_id=${placeInfo.id}');
    final decoded = json.decode(response.body);
    if (decoded[0]['name'] != null) {
      setState(() {
        imageName = decoded[0]['name'];
      });
    }
    setState(() {
      calledForImage = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    placeInfo = ModalRoute.of(context).settings.arguments;
    if (!calledForImage) getImage();

    return Scaffold(
        appBar: AppBar(
          title: Text(placeInfo.name),
          actions: <Widget>[
            if (accessLevel.admin)
              IconButton(
                icon: Icon(
                  Icons.edit,
                  color: Colors.white,
                ),
                onPressed: openEditor,
              )
          ],
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.all(12.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Center(
                  child: Container(
                      child: ClipRRect(
                    borderRadius: BorderRadius.circular(12.0),
                    child: imageName == null
                        ? Container()
                        : Image.network(
                            'https://spoznavacka.rohrovi.cz/toursman_img/$imageName',
                            height: 216.0,
                          ),
                  )),
                ),
                Center(
                  child: Container(
                      margin: EdgeInsets.only(top: 12.0), child: buildIcon()),
                ),
                Center(
                    child: Text(
                  placeInfo.name,
                  style: TextStyle(fontSize: 32.0),
                )),
                Center(
                    child: Container(
                  margin: EdgeInsets.only(top: 12.0),
                  child: Text(placeInfo.description, style: TextStyle(fontSize: 20.0),),
                ))
              ],
            ),
          ),
        ));
  }

  void openEditor() async {
    await Navigator.of(context).pushNamed('/placeEdit', arguments: placeInfo);
    Navigator.pop(context);
  }

  Icon buildIcon() {
    double iconSize = 64.0;
    switch (placeInfo.categoryId) {
      case 1:
        return Icon(
          Icons.local_florist,
          color: Colors.green,
          size: iconSize,
        );
        break;
      case 2:
        return Icon(
          Icons.account_balance,
          color: Colors.orange,
          size: iconSize,
        );
        break;
      case 3:
        return Icon(
          Icons.wc,
          color: Colors.blueAccent,
          size: iconSize,
        );
        break;
      case 4:
        return Icon(
          Icons.restaurant,
          color: Colors.amber,
          size: iconSize,
        );
        break;
      case 5:
        return Icon(
          Icons.bookmark,
          color: Colors.brown,
          size: iconSize,
        );
        break;
      default:
        return Icon(
          Icons.place,
          color: Colors.red,
          size: iconSize,
        );
        break;
    }
  }
}
