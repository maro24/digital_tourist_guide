import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map/plugin_api.dart';
import 'package:latlong/latlong.dart';
import 'package:location/location.dart';
import 'package:map_controller/map_controller.dart';
import 'package:tourguide/models/place.dart';
import 'package:tourguide/controllers/place_controller.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:tourguide/utils/access_level.dart';
import 'package:tourguide/utils/map_position_response.dart';
import 'package:tourguide/utils/filter_settings.dart';

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
}

class HomePageState extends State<HomePage> with WidgetsBindingObserver {
  final TextStyle _drawerItemStyle = new TextStyle(color: Colors.black);
  final Color adminModeColor = Colors.amber;
  LatLng _currentLatLng;
  var location = new Location();
  FlutterMap flutterMap;
  List<Marker> markersList = new List();
  final secureStorage = new FlutterSecureStorage();

  MapController mapController;
  StatefulMapController statefulMapController;
  StreamSubscription<StatefulMapControllerStateChange> mapSub;
  bool mapReady = false;

  StreamSubscription<LocationData> subscription;
  bool subscriptionRunning = false;

  Marker deviceLocation;
  double zoomLevel = 13.0;
  bool firstLocation = true;
  int _menuPresses = 0;

  List<LatLng> areaBoundaryPoints = new List();
  List<Place> places = new List();

  AccessLevel accessLevel = AccessLevel();
  FilterSettings filterSettings = FilterSettings();

  List<String> markerNames = new List();
  bool locationGranted = false;

  bool currentlyChecking = false;

  //TODO: keep tracking target or not (bool)
  //bool trackingPosition = true;

  @override
  void initState() {
    mapController = new MapController();
    statefulMapController =
        new StatefulMapController(mapController: mapController);
    statefulMapController.onReady.then((_) => mapReady = true);
    mapSub =
        statefulMapController.changeFeed.listen((change) => setState(() {}));

    super.initState();

    getAdminModeSettings();

    initializePlaces();

    startLocating();

    WidgetsBinding.instance.addObserver(this);
  }

  void getAdminModeSettings() async {
    if (await secureStorage.read(key: 'mode') == 'admin') {
      setState(() {
        accessLevel.admin = true;
      });
    } else {
      setState(() {
        accessLevel.admin = false;
      });
    }
  }

  void menuPressed() {
    if (accessLevel.admin) {
      _menuPresses = 0;
    } else {
      _menuPresses++;
      if (_menuPresses >= 6) {
        TextEditingController _pwdController = new TextEditingController();
        showDialog(
            context: context,
            barrierDismissible: false,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text('Admin'),
                content: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text('Zadejte heslo pro vstup do režimu Admin'),
                    Container(
                        margin: EdgeInsets.only(top: 12.0),
                        child: TextField(
                          obscureText: true,
                          style: TextStyle(color: Colors.black),
                          controller: _pwdController,
                          decoration: InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Theme.of(context).primaryColor,
                                      width: 2.0)),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Theme.of(context).primaryColor,
                                      width: 2.0)),
                              labelStyle: TextStyle(
                                  color: Theme.of(context).primaryColor),
                              labelText: 'Heslo'),
                        ))
                  ],
                ),
                actions: <Widget>[
                  FlatButton(
                    child: Text(
                      'Zrušit',
                      style: TextStyle(color: Colors.grey),
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                  FlatButton(
                    child: Text(
                      'Potvrdit',
                      style: TextStyle(color: Colors.green),
                    ),
                    onPressed: () {
                      if (_pwdController.text == '77eDit0R13') {
                        secureStorage.write(key: 'mode', value: 'admin');
                        setState(() {
                          accessLevel.admin = true;
                        });
                      }
                      Navigator.pop(context);
                    },
                  )
                ],
              );
            });
        _menuPresses = 0;
      }
    }
  }

  void initializePlaces() async {
    var result = await Places.getPlacesList();
    places = result;
    refreshLocations();
  }

  void refreshLocations() async {
    statefulMapController.removeMarkers(names: markerNames);
    markerNames.clear();
    for (final place in places) {
      if (filterSettings.areaIds[place.areaId] == null ||
          filterSettings.areaIds[place.areaId] == true) {
        if (filterSettings.categoryIds[place.categoryId] == null ||
            filterSettings.categoryIds[place.categoryId] == true) {
          markerNames.add(place.name);
          switch (place.categoryId) {
            //Priroda
            case 1:
              statefulMapController.addMarker(
                  marker: new Marker(
                      width: 200.0,
                      height: 200.0,
                      point: new LatLng(place.lat, place.lon),
                      builder: (ctx) => new GestureDetector(
                          onTap: () {
                            openPlaceInfo(place);
                          },
                          child:
                              Icon(Icons.local_florist, color: Colors.green))),
                  name: place.name);
              break;
            //Architektura
            case 2:
              statefulMapController.addMarker(
                  marker: new Marker(
                      width: 200.0,
                      height: 200.0,
                      point: new LatLng(place.lat, place.lon),
                      builder: (ctx) => GestureDetector(
                          onTap: () {
                            openPlaceInfo(place);
                          },
                          child: new Icon(Icons.account_balance,
                              color: Colors.orange))),
                  name: place.name);
              break;
            //WC
            case 3:
              statefulMapController.addMarker(
                  marker: new Marker(
                      width: 200.0,
                      height: 200.0,
                      point: new LatLng(place.lat, place.lon),
                      builder: (ctx) => GestureDetector(
                          onTap: () {
                            openPlaceInfo(place);
                          },
                          child: new Icon(Icons.wc, color: Colors.blueAccent))),
                  name: place.name);
              break;
            //Obcerstveni
            case 4:
              statefulMapController.addMarker(
                  marker: new Marker(
                      width: 200.0,
                      height: 200.0,
                      point: new LatLng(place.lat, place.lon),
                      builder: (ctx) => GestureDetector(
                          onTap: () {
                            openPlaceInfo(place);
                          },
                          child:
                              new Icon(Icons.restaurant, color: Colors.amber))),
                  name: place.name);
              break;
            case 5:
              statefulMapController.addMarker(
                  marker: new Marker(
                      width: 200.0,
                      height: 200.0,
                      point: new LatLng(place.lat, place.lon),
                      builder: (ctx) => GestureDetector(
                          onTap: () {
                            openPlaceInfo(place);
                          },
                          child:
                              new Icon(Icons.bookmark, color: Colors.brown))),
                  name: place.name);
              break;
            default:
              statefulMapController.addMarker(
                  marker: new Marker(
                      width: 200.0,
                      height: 200.0,
                      point: new LatLng(place.lat, place.lon),
                      builder: (ctx) =>
                          new Icon(Icons.place, color: Colors.red)),
                  name: place.name);
              break;
          }
        }
      }
    }
    statefulMapController.addMarker(
        marker: new Marker(
          width: 200.0,
          height: 200.0,
          point: _currentLatLng,
          builder: (ctx) => new Container(
            child:
                new Icon(Icons.my_location, color: Colors.blue.withOpacity(.6)),
          ),
        ),
        name: 'currentLocation');
  }

  @override
  void dispose() {
    subscription.cancel();
    mapSub.cancel();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      stopLocating();
      subscriptionRunning = false;
    } else if (state == AppLifecycleState.resumed) {
      if (!subscriptionRunning) startLocating();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Tourist guide'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.filter_list),
            tooltip: 'Filtry',
            onPressed: () {
              openFiltersPage();
            },
          )
        ],
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.all(0.0),
          children: <Widget>[
            Container(
              height: 150.0,
              child: DrawerHeader(
                decoration: BoxDecoration(color: Colors.green),
                child: Center(
                    child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Container(
                        child: InkWell(
                            onTap: menuPressed,
                            child: Text(
                              'Menu',
                              style: TextStyle(
                                  color: Colors.white, fontSize: 26.0),
                            ))),
                    if (accessLevel.admin)
                      Container(
                          padding: EdgeInsets.all(6.0),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(18.0),
                              border: Border.all(
                                  width: 2.0, color: adminModeColor)),
                          margin: EdgeInsets.only(top: 6.0),
                          child: Text('Admin',
                              style: TextStyle(
                                color: adminModeColor,
                                fontSize: 18.0,
                              )))
                  ],
                )),
              ),
            ),
            ListTile(
                leading: Icon(Icons.format_list_bulleted),
                title: Text('Oblasti', style: _drawerItemStyle),
                onTap: () {
                  openAreaList();
                }),
            Divider(
              thickness: 1.0,
            ),
            if (accessLevel.admin)
              ListTile(
                leading: Icon(Icons.remove_circle_outline),
                title: Text(
                  'Opustit režim Admin',
                  style: _drawerItemStyle,
                ),
                onTap: () {
                  secureStorage.write(key: 'mode', value: 'user');
                  setState(() {
                    accessLevel.admin = false;
                  });
                },
              )
          ],
        ),
      ),
      body: Container(
          child: Stack(
        children: <Widget>[
          flutterMap = new FlutterMap(
            mapController: mapController,
            options: MapOptions(
                center: _currentLatLng != null
                    ? _currentLatLng
                    : new LatLng(49.8175, 15.4730),
                zoom: _currentLatLng != null ? 11.0 : 6.0,
                minZoom: 6.0,
                maxZoom: 18.0),
            layers: [
              new TileLayerOptions(
                  urlTemplate:
                      "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                  subdomains: ['a', 'b', 'c']),
              areaBoundaryPoints.length > 0
                  ? new PolygonLayerOptions(polygons: [
                      new Polygon(
                          points: areaBoundaryPoints,
                          color: Colors.green.withOpacity(.3),
                          borderColor: Colors.green,
                          borderStrokeWidth: .5)
                    ])
                  : new PolygonLayerOptions(),
              new MarkerLayerOptions(markers: statefulMapController.markers),
            ],
          ),
          areaBoundaryPoints.length > 0
              ? Align(
                  alignment: Alignment.topRight,
                  child: Container(
                    margin: EdgeInsets.all(16.0),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Theme.of(context).primaryColor),
                    child: IconButton(
                        icon: Icon(
                          Icons.block,
                          color: Colors.red,
                        ),
                        onPressed: () {
                          setState(() {
                            areaBoundaryPoints.clear();
                          });
                        }),
                  ),
                )
              : Container(
                  height: 0.0,
                  width: 0.0,
                )
        ],
      )),
      //Re-centers map to user
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          if (accessLevel.admin)
            Container(
                margin: EdgeInsets.only(bottom: 14.0),
                child: FloatingActionButton(
                  heroTag: null,
                  onPressed: () {
                    openPlaceEditor();
                  },
                  child: Icon(
                    Icons.add,
                    color: Colors.white,
                  ),
                  backgroundColor: Colors.green,
                )),
          if (locationGranted)
            FloatingActionButton(
              heroTag: null,
              onPressed: () {
                mapController.move(_currentLatLng, statefulMapController.zoom);
              },
              child: Icon(
                Icons.my_location,
                color: Colors.white,
              ),
              backgroundColor: Colors.green,
            )
        ],
      ),
    );
  }

  void openPlaceInfo(Place place) async {
    stopLocating();
    Place tempPlace = new Place();
    tempPlace = place;
    await Navigator.of(context).pushNamed('/placeView', arguments: tempPlace);
    initializePlaces();
    startLocating();
  }

  void openFiltersPage() async {
    stopLocating();
    await Navigator.of(context).pushNamed('/filters');
    initializePlaces();
    startLocating();
  }

  Future<void> checkLocationPermission() async {
    if (!currentlyChecking) {
      setState(() {
        currentlyChecking = true;
      });
      PermissionStatus permission = await PermissionHandler()
          .checkPermissionStatus(PermissionGroup.location);
      if (permission == PermissionStatus.granted) {
        setState(() {
          locationGranted = true;
        });
      } else {
        setState(() {
          locationGranted = false;
        });
        Map<PermissionGroup, PermissionStatus> permissions =
            await PermissionHandler()
                .requestPermissions([PermissionGroup.location]);
        if (permissions[PermissionGroup.location] == PermissionStatus.granted) {
          setState(() {
            locationGranted = true;
          });
        } else {
          setState(() {
            locationGranted = false;
          });
        }
      }
      setState(() {
        currentlyChecking = false;
      });
    }
  }

  void startLocating() async {
    await checkLocationPermission();
    if (locationGranted && !subscriptionRunning) {
      subscription =
          location.onLocationChanged().listen((LocationData currentLocation) {
        setState(() {
          _currentLatLng =
              new LatLng(currentLocation.latitude, currentLocation.longitude);
          refreshLocations();
          if (firstLocation) {
            mapController.move(_currentLatLng, 13.0);
            firstLocation = false;
          }
        });
      });
      setState(() {
        subscriptionRunning = true;
      });
    }
  }

  void stopLocating() {
    subscription.cancel();
    subscriptionRunning = false;
  }

  void openPlaceEditor() async {
    stopLocating();
    MapPositionResponse mapPositionResponse =
        new MapPositionResponse(statefulMapController.center);
    await Navigator.pushNamed(context, '/placeEdit',
        arguments: mapPositionResponse);
    initializePlaces();
    startLocating();
  }

  void openAreaList() async {
    stopLocating();
    var boundaryPointsJson = await Navigator.of(context).pushNamed('/areaList');
    //areaBoundaryPoints = await Navigator.of(context).pushNamed('/areaList');
    List<LatLng> list = new List();
    if (boundaryPointsJson != null) {
      for (final position in boundaryPointsJson) {
        list.add(new LatLng(
            double.parse(position['lat']), double.parse(position['lon'])));
      }
      setState(() {
        areaBoundaryPoints = list;
      });
    }
    Navigator.of(context).pop();
    initializePlaces();
    startLocating();
  }
}
