import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:map_controller/map_controller.dart';
import 'package:latlong/latlong.dart';
import 'package:tourguide/utils/map_position_response.dart';
import 'package:location/location.dart';

class MapPlaceSelectorPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MapPlaceSelectorPageState();
  }
}

class MapPlaceSelectorPageState extends State<MapPlaceSelectorPage> {
  FlutterMap flutterMap;
  MapController mapController = new MapController();
  StatefulMapController statefulMapController;
  LatLng markerPosition = new LatLng(50.209987, 15.832787);
  LatLng _currentLatLng = new LatLng(50.209987, 15.832787);
  StreamSubscription<StatefulMapControllerStateChange> mapSub;
  bool mapReady = false;
  MapPositionResponse mapPositionResponse;

  @override
  void initState() {
    super.initState();

    statefulMapController =
        new StatefulMapController(mapController: mapController);
    statefulMapController.onReady.then((_) {
      mapReady = true;
      centerMap();
    });
  }

  void centerMap() async {
    if (mapPositionResponse.latLng != null) {
      setState(() {
        statefulMapController.centerOnPoint(mapPositionResponse.latLng);
        statefulMapController.zoomTo(18.0);
      });
    } else {
      Location location = new Location();
      LocationData locationData;
      locationData = await location.getLocation();
      setState(() {
        statefulMapController.centerOnPoint(
            new LatLng(locationData.latitude, locationData.longitude));
      });
    }
  }

  @override
  void dispose() {
    mapSub.cancel();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    mapPositionResponse = ModalRoute.of(context).settings.arguments;

    return Scaffold(
        appBar: AppBar(
          title: Text('Map place selector'),
        ),
        body: Container(
            child: Stack(
          children: <Widget>[
            flutterMap = new FlutterMap(
              mapController: mapController,
              options: MapOptions(
                  onPositionChanged: (position, hasGesture) {
                    updatePosition(position, hasGesture);
                  },
                  center: _currentLatLng,
                  zoom: 11.0,
                  minZoom: 6.0,
                  maxZoom: 18.0),
              layers: [
                new TileLayerOptions(
                    urlTemplate:
                        "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                    subdomains: ['a', 'b', 'c']),
                new MarkerLayerOptions(markers: [
                  new Marker(
                    width: 200.0,
                    height: 200.0,
                    point: markerPosition,
                    builder: (ctx) => new Container(
                      child: new Icon(Icons.place,
                          color: Colors.red.withOpacity(.8)),
                    ),
                  )
                ])
              ],
            ),
          ],
        )),
        //Re-centers map to user
        floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
        floatingActionButton: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Container(
                margin: EdgeInsets.only(bottom: 14.0),
                child: FloatingActionButton(
                  heroTag: null,
                  onPressed: () {
                    returnPosition();
                  },
                  child: Icon(
                    Icons.check,
                    color: Colors.white,
                  ),
                  backgroundColor: Colors.green,
                )),
            FloatingActionButton(
              heroTag: null,
              onPressed: () {
                mapController.move(_currentLatLng, 15.0);
              },
              child: Icon(
                Icons.my_location,
                color: Colors.white,
              ),
              backgroundColor: Colors.green,
            )
          ],
        ));
  }

  void updatePosition(MapPosition position, bool hasGesture) {
    if (mapReady) {
      setState(() {
        markerPosition = position.center;
      });
    }
  }

  void returnPosition() {
    this.mapPositionResponse.latLng = markerPosition;
    Navigator.pop(context);
  }
}
