import 'dart:io';
import 'dart:async';
import 'package:async/async.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:location/location.dart';
import 'package:tourguide/controllers/category_controller.dart';
import 'package:tourguide/controllers/area_controller.dart';
import 'package:tourguide/controllers/image_controller.dart';
import 'package:tourguide/controllers/place_controller.dart';
import 'package:tourguide/models/category.dart';
import 'package:tourguide/models/area.dart';
import 'package:tourguide/models/image_model.dart';
import 'package:tourguide/models/place.dart';
import 'package:tourguide/utils/map_position_response.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:convert';

class PlaceEditorPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _PlaceEditorPageState();
  }
}

class _PlaceEditorPageState extends State<PlaceEditorPage> {
  double cardMargin = 12.0;
  String areaValue;
  String categoryValue;
  String accuracyMessage = '';

  var categories;
  var areas;

  bool uploading = false;

  bool locating = false;
  var locationListener;
  var location = new Location();

  var nameController = new TextEditingController();
  var latController = new TextEditingController();
  var lonController = new TextEditingController();
  var descriptionController = new TextEditingController();
  File image;

  Place place;
  LatLng positionFromHomeMap;

  bool calledForImage = false;
  String imageName;

  bool loaded = false;

  @override
  void initState() {
    super.initState();

    initializeLists();
  }

  void initializeLists() async {
    var categoriesDownload = await Categories.getCategoriesList();
    var areasDownload = await Areas.getAreasList();
    setState(() {
      categories = categoriesDownload;
      areas = areasDownload;
      if (place != null) {
        this.areaValue = getAreaNameById(place.areaId);
        this.categoryValue = getCategoryNameById(place.categoryId);
      }
    });
  }

  //Locating x not locating Row children
  List<Widget> buildChildren() {
    if (locating) {
      var builder = [
        Container(
            child: new MaterialButton(
          color: Colors.grey[300],
          child: Text('Stop'),
          onPressed: () {
            setState(() {
              locationListener.cancel();
              locating = false;
            });
          },
        )),
        Container(
            margin: EdgeInsets.only(left: cardMargin),
            child: new CircularProgressIndicator(
              valueColor:
                  AlwaysStoppedAnimation<Color>(Theme.of(context).primaryColor),
            )),
        Container(
          margin: EdgeInsets.only(left: cardMargin),
          child: Text(accuracyMessage),
        )
      ];
      return builder;
    } else {
      var builder = [
        Container(
          child: new MaterialButton(
            color: Colors.grey[300],
            child: Text('Lokalizovat'),
            onPressed: uploading
                ? null
                : () {
                    locationListener = location
                        .onLocationChanged()
                        .listen((LocationData currentLocation) {
                      setState(() {
                        accuracyMessage =
                            'Přesnost: ${currentLocation.accuracy.toStringAsFixed(1)} > 10 m';
                      });
                      if (currentLocation.accuracy <= 10.0) {
                        setState(() {
                          latController.text =
                              currentLocation.latitude.toString();
                          lonController.text =
                              currentLocation.longitude.toString();
                        });
                        locationListener.cancel();
                        locating = false;
                      }
                    });
                    setState(() {
                      locating = true;
                    });
                  },
          ),
        ),
        Container(
          margin: EdgeInsets.only(left: 12.0),
          child: new MaterialButton(
            color: Colors.grey[300],
            child: Text('Mapa'),
            onPressed: uploading
                ? null
                : () {
                    openMapPlaceSelector();
                  },
          ),
        )
      ];
      return builder;
    }
  }

  void getImageName() async {
    setState(() {
      calledForImage = true;
    });
    String tempName = await Images.getImageNameByPlaceId(place.id);
    if (tempName != null) {
      setState(() {
        imageName = tempName;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    if (ModalRoute.of(context).settings.arguments != null) {
      if (ModalRoute.of(context).settings.arguments is Place) {
        place = ModalRoute.of(context).settings.arguments;
        if (place != null && !loaded) {
          setState(() {
            this.nameController.text = place.name;
            this.latController.text = place.lat.toString();
            this.lonController.text = place.lon.toString();
            this.descriptionController.text = place.description;
            loaded = true;
          });
        }
      } else if (ModalRoute.of(context).settings.arguments
          is MapPositionResponse) {
        place = null;
        MapPositionResponse _mapPositionResponse =
            ModalRoute.of(context).settings.arguments;
        setState(() {
          positionFromHomeMap = _mapPositionResponse.latLng;
        });
      } else {
        place = null;
      }
    } else {
      place = null;
    }

    if (!calledForImage && place != null) {
      getImageName();
    }

    return Scaffold(
        appBar: AppBar(
          title: Text('Editor'),
          actions: <Widget>[
            if (place != null)
              uploading || locating
                  ? IconButton(
                      icon: Icon(
                        Icons.delete_forever,
                        color: Colors.grey,
                      ),
                      onPressed: null,
                    )
                  : IconButton(
                      icon: Icon(
                        Icons.delete_forever,
                        color: Colors.red,
                      ),
                      onPressed: deletePlace,
                    ),
            uploading || locating
                ? IconButton(
                    icon: Icon(
                      Icons.file_upload,
                      color: Colors.grey,
                    ),
                    onPressed: null,
                  )
                : IconButton(
                    icon: Icon(Icons.file_upload),
                    onPressed: place == null ? uploadPlace : updatePlace,
                  )
          ],
        ),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: SingleChildScrollView(
              child: place == null
                  ? Container(
                      child: Column(
                      children: <Widget>[
                        if (uploading)
                          Container(
                            padding: EdgeInsets.only(
                                top: cardMargin, bottom: cardMargin),
                            child: CircularProgressIndicator(
                              valueColor: AlwaysStoppedAnimation<Color>(
                                  Theme.of(context).primaryColor),
                            ),
                          ),
                        //Name section
                        Container(
                          margin: EdgeInsets.only(
                              top: cardMargin,
                              left: cardMargin,
                              right: cardMargin),
                          child: Column(
                            children: <Widget>[
                              TextField(
                                enabled: uploading ? false : true,
                                controller: nameController,
                                maxLines: null,
                                obscureText: false,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  labelText: 'Název',
                                ),
                              )
                            ],
                          ),
                        ),
                        //Location section
                        Container(
                          margin: EdgeInsets.only(
                              top: cardMargin,
                              left: cardMargin,
                              right: cardMargin),
                          child: Column(
                            children: <Widget>[
                              //Locate controls
                              Container(
                                child: Row(children: buildChildren()),
                              ),
                              //Lat input
                              Container(
                                  margin: EdgeInsets.only(top: cardMargin),
                                  child: TextField(
                                    enabled: uploading ? false : true,
                                    controller: latController,
                                    maxLines: null,
                                    obscureText: false,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      labelText: 'Lat',
                                    ),
                                  )),
                              //Lon input
                              Container(
                                  margin: EdgeInsets.only(top: cardMargin),
                                  child: TextField(
                                    enabled: uploading ? false : true,
                                    controller: lonController,
                                    maxLines: null,
                                    obscureText: false,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      labelText: 'Lon',
                                    ),
                                  ))
                            ],
                          ),
                        ),
                        //Selection section
                        Container(
                          margin: EdgeInsets.only(
                              top: cardMargin,
                              left: cardMargin,
                              right: cardMargin),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Text('Oblast: '),
                                  areas != null
                                      ? DropdownButton<String>(
                                          value: areaValue,
                                          hint: Text('Vybrat'),
                                          iconSize: 24,
                                          elevation: 16,
                                          style: TextStyle(color: Colors.black),
                                          underline: Container(
                                            height: 2,
                                            color:
                                                Theme.of(context).primaryColor,
                                          ),
                                          onChanged: (String newValue) {
                                            setState(() {
                                              areaValue = newValue;
                                            });
                                          },
                                          items: areas
                                              .map<DropdownMenuItem<String>>(
                                                  (Area value) {
                                            return DropdownMenuItem<String>(
                                              value: value.name,
                                              child: Text(value.name),
                                            );
                                          }).toList(),
                                        )
                                      : Container(
                                          margin:
                                              EdgeInsets.only(left: cardMargin),
                                          child: CircularProgressIndicator(
                                              valueColor:
                                                  AlwaysStoppedAnimation<Color>(
                                                      Theme.of(context)
                                                          .primaryColor)))
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Text('Kategorie: '),
                                  categories != null
                                      ? DropdownButton<String>(
                                          value: categoryValue,
                                          hint: Text('Vybrat'),
                                          iconSize: 24,
                                          elevation: 16,
                                          style: TextStyle(color: Colors.black),
                                          underline: Container(
                                            height: 2,
                                            color:
                                                Theme.of(context).primaryColor,
                                          ),
                                          onChanged: (String newValue) {
                                            setState(() {
                                              categoryValue = newValue;
                                            });
                                          },
                                          items: categories
                                              .map<DropdownMenuItem<String>>(
                                                  (Category value) {
                                            return DropdownMenuItem<String>(
                                              value: value.name,
                                              child: Text(value.name),
                                            );
                                          }).toList(),
                                        )
                                      : Container(
                                          margin:
                                              EdgeInsets.only(left: cardMargin),
                                          child: CircularProgressIndicator(
                                              valueColor:
                                                  AlwaysStoppedAnimation<Color>(
                                                      Theme.of(context)
                                                          .primaryColor)))
                                ],
                              )
                            ],
                          ),
                        ),
                        //Image section
                        Container(
                          margin: EdgeInsets.all(24.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              image == null
                                  ? Container(
                                      child: Icon(
                                        Icons.image,
                                        color: Colors.grey,
                                      ),
                                    )
                                  : Container(
                                      child: Image.file(
                                        image,
                                        height: 120.0,
                                      ),
                                    ),
                              image == null
                                  ? Container(
                                      child: IconButton(
                                        icon: Icon(Icons.add_circle),
                                        color: Colors.green,
                                        onPressed: selectImage,
                                      ),
                                    )
                                  : Container(
                                      child: IconButton(
                                        icon: Icon(Icons.find_replace),
                                        color: Colors.green,
                                        onPressed: selectImage,
                                      ),
                                    ),
                              if (image != null)
                                Container(
                                  child: IconButton(
                                    icon: Icon(Icons.delete_forever),
                                    color: Colors.red,
                                    onPressed: () {
                                      unlinkImage(false);
                                    },
                                  ),
                                ),
                            ],
                          ),
                        ),
                        //Description section
                        Container(
                          margin: EdgeInsets.only(
                              top: cardMargin,
                              left: cardMargin,
                              right: cardMargin),
                          child: Column(
                            children: <Widget>[
                              TextField(
                                enabled: uploading ? false : true,
                                controller: descriptionController,
                                maxLines: null,
                                obscureText: false,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  labelText: 'Popis',
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ))
                  : Container(
                      child: Column(
                      children: <Widget>[
                        if (uploading)
                          Container(
                            padding: EdgeInsets.only(
                                top: cardMargin, bottom: cardMargin),
                            child: CircularProgressIndicator(
                              valueColor: AlwaysStoppedAnimation<Color>(
                                  Theme.of(context).primaryColor),
                            ),
                          ),
                        //Name section
                        Container(
                          margin: EdgeInsets.only(
                              top: cardMargin,
                              left: cardMargin,
                              right: cardMargin),
                          child: Column(
                            children: <Widget>[
                              TextField(
                                enabled: uploading ? false : true,
                                controller: nameController,
                                maxLines: null,
                                obscureText: false,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  labelText: 'Název',
                                ),
                              )
                            ],
                          ),
                        ),
                        //Location section
                        Container(
                          margin: EdgeInsets.only(
                              top: cardMargin,
                              left: cardMargin,
                              right: cardMargin),
                          child: Column(
                            children: <Widget>[
                              //Locate controls
                              Container(
                                child: Row(children: buildChildren()),
                              ),
                              //Lat input
                              Container(
                                  margin: EdgeInsets.only(top: cardMargin),
                                  child: TextField(
                                    enabled: uploading ? false : true,
                                    controller: latController,
                                    maxLines: null,
                                    obscureText: false,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      labelText: 'Lat',
                                    ),
                                  )),
                              //Lon input
                              Container(
                                  margin: EdgeInsets.only(top: cardMargin),
                                  child: TextField(
                                    enabled: uploading ? false : true,
                                    controller: lonController,
                                    maxLines: null,
                                    obscureText: false,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      labelText: 'Lon',
                                    ),
                                  ))
                            ],
                          ),
                        ),
                        //Selection section
                        Container(
                          margin: EdgeInsets.only(
                              top: cardMargin,
                              left: cardMargin,
                              right: cardMargin),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Text('Oblast: '),
                                  areas != null
                                      ? DropdownButton<String>(
                                          value: areaValue,
                                          hint: Text('Vybrat'),
                                          iconSize: 24,
                                          elevation: 16,
                                          style: TextStyle(color: Colors.black),
                                          underline: Container(
                                            height: 2,
                                            color:
                                                Theme.of(context).primaryColor,
                                          ),
                                          onChanged: (String newValue) {
                                            setState(() {
                                              areaValue = newValue;
                                            });
                                          },
                                          items: areas
                                              .map<DropdownMenuItem<String>>(
                                                  (Area value) {
                                            return DropdownMenuItem<String>(
                                              value: value.name,
                                              child: Text(value.name),
                                            );
                                          }).toList(),
                                        )
                                      : Container(
                                          margin:
                                              EdgeInsets.only(left: cardMargin),
                                          child: CircularProgressIndicator(
                                              valueColor:
                                                  AlwaysStoppedAnimation<Color>(
                                                      Theme.of(context)
                                                          .primaryColor)))
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Text('Kategorie: '),
                                  categories != null
                                      ? DropdownButton<String>(
                                          value: categoryValue,
                                          hint: Text('Vybrat'),
                                          iconSize: 24,
                                          elevation: 16,
                                          style: TextStyle(color: Colors.black),
                                          underline: Container(
                                            height: 2,
                                            color:
                                                Theme.of(context).primaryColor,
                                          ),
                                          onChanged: (String newValue) {
                                            setState(() {
                                              categoryValue = newValue;
                                            });
                                          },
                                          items: categories
                                              .map<DropdownMenuItem<String>>(
                                                  (Category value) {
                                            return DropdownMenuItem<String>(
                                              value: value.name,
                                              child: Text(value.name),
                                            );
                                          }).toList(),
                                        )
                                      : Container(
                                          margin:
                                              EdgeInsets.only(left: cardMargin),
                                          child: CircularProgressIndicator(
                                              valueColor:
                                                  AlwaysStoppedAnimation<Color>(
                                                      Theme.of(context)
                                                          .primaryColor)))
                                ],
                              )
                            ],
                          ),
                        ),
                        //Image section
                        Container(
                          margin: EdgeInsets.all(24.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              buildImageIndicator(),
                              buildAddImage(),
                              buildDeleteImage()
                            ],
                          ),
                        ),
                        //Description section
                        Container(
                          margin: EdgeInsets.only(
                              top: cardMargin,
                              left: cardMargin,
                              right: cardMargin),
                          child: Column(
                            children: <Widget>[
                              TextField(
                                enabled: uploading ? false : true,
                                controller: descriptionController,
                                maxLines: null,
                                obscureText: false,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  labelText: 'Popis',
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ))),
        ));
  }

  Container buildImageIndicator() {
    if (image == null && imageName == null)
      return Container(
          child: Icon(
        Icons.image,
        color: Colors.grey,
      ));
    else if (image != null)
      return Container(
          child: Image.file(
        image,
        height: 120.0,
      ));
    else if (imageName != null)
      return Container(
          child: Image.network(
        'https://spoznavacka.rohrovi.cz/toursman_img/$imageName',
        height: 120.0,
      ));
  }

  Container buildAddImage() {
    return image == null && imageName == null
        ? Container(
            child: IconButton(
              icon: Icon(Icons.add_circle),
              color: Colors.green,
              onPressed: selectImage,
            ),
          )
        : Container(
            child: IconButton(
              icon: Icon(Icons.find_replace),
              color: Colors.green,
              onPressed: selectImage,
            ),
          );
  }

  Container buildDeleteImage() {
    if (image == null && imageName == null)
      return Container();
    else
      return Container(
        child: IconButton(
          icon: Icon(Icons.delete_forever),
          color: Colors.red,
          onPressed: () {
            unlinkImage(true);
          },
        ),
      );
  }

  void unlinkImage(bool existsInDb) async {
    if (existsInDb) {
      await Images.unlinkImageInDb(place.id);
      setState(() {
        image = null;
        imageName = null;
      });
    } else {
      setState(() {
        image = null;
        imageName = null;
      });
    }
  }

  Future<void> selectImage() async {
    var tempImage = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      image = tempImage;
    });
  }

  Future<void> uploadImage(int placeId) async {
    var stream = new http.ByteStream(DelegatingStream.typed(image.openRead()));
    var length = await image.length();

    var uri = Uri.parse(
        'https://spoznavacka.rohrovi.cz/toursman_demo_api/api1/upload_img.php');

    var request = new http.MultipartRequest("POST", uri);

    DateTime currentTimeTemp = DateTime.now();
    String imageFileName =
        '${currentTimeTemp.toString().split(' ')[0]}_${currentTimeTemp.toString().split(' ')[1].split('.')[0].replaceAll(':', '-')}.${image.path.split('.')[1]}';

    var multipartFile = new http.MultipartFile('uploaded_file', stream, length,
        filename: imageFileName);

    request.files.add(multipartFile);

    var response = await request.send();

    response.stream.transform(utf8.decoder).listen((value) {
      if (value == 'success') {
        ImageModel imageModel = new ImageModel(imageFileName, placeId);
        insertImageToDb(imageModel);
      }
    });
  }

  Future<void> insertImageToDb(ImageModel imageModel) async {
    await Images.insertImageToDb(imageModel);
  }

  void updatePlace() async {
    setState(() {
      uploading = true;
    });
    int placeId = place.id;
    place = new Place(
        id: placeId,
        name: nameController.text,
        lat: double.parse(latController.text),
        lon: double.parse(lonController.text),
        description: descriptionController.text,
        areaId: findAreaIdByName(areaValue),
        categoryId: findCategoryIdByName(categoryValue));
    await Places.updatePlace(place);
    if (image != null) {
      await Images.unlinkImageInDb(place.id);
      await uploadImage(place.id);
      setState(() {
        uploading = false;
      });
      Navigator.of(context).pop();
    } else {
      setState(() {
        uploading = false;
      });
      Navigator.of(context).pop();
    }
  }

  void deletePlace() async {
    await Places.deletePlace(place.id);
    Navigator.pop(context);
  }

  String getAreaNameById(int id) {
    for (final area in areas) {
      if (area.id == id) return area.name;
    }
    return 'Error';
  }

  String getCategoryNameById(int id) {
    for (final category in categories) {
      if (category.id == id) return category.name;
    }
    return 'Error';
  }

  void openMapPlaceSelector() async {
    if (place != null) {
      MapPositionResponse mapPositionResponse =
          new MapPositionResponse(new LatLng(place.lat, place.lon));
      var result = await Navigator.pushNamed(context, '/mapSelector',
          arguments: mapPositionResponse);
      setState(() {
        latController.text = mapPositionResponse.latLng.latitude.toString();
        lonController.text = mapPositionResponse.latLng.longitude.toString();
      });
    } else if (positionFromHomeMap != null) {
      MapPositionResponse mapPositionResponse =
          new MapPositionResponse(positionFromHomeMap);
      var result = await Navigator.pushNamed(context, '/mapSelector',
          arguments: mapPositionResponse);
      setState(() {
        latController.text = mapPositionResponse.latLng.latitude.toString();
        lonController.text = mapPositionResponse.latLng.longitude.toString();
      });
    } else {
      MapPositionResponse mapPositionResponse = new MapPositionResponse(null);
      var result = await Navigator.pushNamed(context, '/mapSelector',
          arguments: mapPositionResponse);
      setState(() {
        latController.text = mapPositionResponse.latLng.latitude.toString();
        lonController.text = mapPositionResponse.latLng.longitude.toString();
      });
    }
  }

  int findCategoryIdByName(String name) {
    for (final category in categories) {
      if (category.name == name) {
        return category.id;
      }
    }
    return 0;
  }

  int findAreaIdByName(String name) {
    for (final area in areas) {
      if (area.name == name) {
        return area.id;
      }
    }
    return 0;
  }

  void uploadPlace() async {
    setState(() {
      uploading = true;
    });
    Place place = new Place(
        name: nameController.text,
        lat: double.parse(latController.text),
        lon: double.parse(lonController.text),
        description: descriptionController.text,
        areaId: findAreaIdByName(areaValue),
        categoryId: findCategoryIdByName(categoryValue));

    int insertedId = await Places.createPlace(place);

    if (image != null) {
      await uploadImage(insertedId);
      setState(() {
        uploading = false;
      });
      Navigator.of(context).pop();
    } else {
      setState(() {
        uploading = false;
      });
      Navigator.of(context).pop();
    }
  }
}
