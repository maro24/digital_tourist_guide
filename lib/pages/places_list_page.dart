import 'package:flutter/material.dart';
import 'package:tourguide/controllers/place_controller.dart';
import 'package:tourguide/models/place.dart';
import 'package:tourguide/utils/route_arguments.dart';
import 'package:tourguide/utils/access_level.dart';

class PlacesListPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return PlacesListPageState();
  }
}

class PlacesListPageState extends State<PlacesListPage> {
  int areaId = 0;
  String areaName = '';
  RouteArguments args;
  AccessLevel accessLevel = AccessLevel();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    args = ModalRoute.of(context).settings.arguments;
    areaId = args.areaId;
    areaName = args.title;

    return Scaffold(
        appBar: AppBar(
          title: Text(areaName),
        ),
        body: Center(
          child: FutureBuilder<List<Place>>(
            future: Places.getPlacesList(),
            builder:
                (BuildContext context, AsyncSnapshot<List<Place>> snapshot) {
              switch (snapshot.connectionState) {
                case ConnectionState.none:
                  return Text('Press button to start.');
                case ConnectionState.active:
                case ConnectionState.waiting:
                  return CircularProgressIndicator();
                case ConnectionState.done:
                  if (snapshot.hasError)
                    return Text('Error: ${snapshot.error}');
                  return Column(children: <Widget>[
                    Expanded(
                      child: ListView.builder(
                          itemCount: snapshot.data.length,
                          itemBuilder: (BuildContext context, int Index) {
                            if (snapshot.data[Index].areaId == this.areaId) {
                              return Card(
                                  child: ListTile(
                                leading: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(snapshot.data[Index].name)
                                  ],
                                ),
                                trailing: accessLevel.admin
                                    ? Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          IconButton(
                                            iconSize: 32.0,
                                            icon: Icon(
                                              Icons.delete,
                                              color: Colors.red,
                                            ),
                                            onPressed: () {
                                              showDialog(
                                                  context: context,
                                                  barrierDismissible: false,
                                                  builder:
                                                      (BuildContext context) {
                                                    return AlertDialog(
                                                      title:
                                                          Text('Odstranit místo'),
                                                      content: Text(
                                                          'Opravdu chcete odstranit toto místo?'),
                                                      actions: <Widget>[
                                                        FlatButton(
                                                          child: Text(
                                                            'Ne',
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .grey),
                                                          ),
                                                          onPressed: () {
                                                            Navigator.pop(
                                                                context);
                                                          },
                                                        ),
                                                        FlatButton(
                                                          child: Text(
                                                            'Ano',
                                                            style: TextStyle(
                                                                color:
                                                                    Colors.red),
                                                          ),
                                                          onPressed: () {
                                                            deletePlace(snapshot
                                                                .data[Index]
                                                                .id);
                                                            Navigator.pop(
                                                                context);
                                                          },
                                                        )
                                                      ],
                                                    );
                                                  });
                                            },
                                          ),
                                        ],
                                      )
                                    : null,
                                onTap: () {
                                  handleClick(snapshot.data[Index]);
                                },
                              ));
                            } else {
                              return Container(
                                width: 0.0,
                                height: 0.0,
                              );
                            }
                          }),
                    )
                  ]);
              }
              return null; // unreachable
            },
          ),
        ));
  }

  void deletePlace(int placeId) async {
    await Places.deletePlace(placeId);
    setState(() {});
  }

  void handleClick(Place place) async {
    Navigator.pushNamed(context, '/placeView', arguments: place);
  }
}
